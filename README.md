# terraform_oci

This is repository for all my oci terraform learning and experiments 

Setup OCI Keys:
Download the generate_keys.sh file from repo
Provide execute permission to the file 
execute it with a name to key ./generate_keys.sh  name_of_key

This will create a key in $HOME/.oci/ folder and shows the public key on terminal console
Copy and paste this on OCI console API keys for the user 