#!/bin/bash
export TF_VAR_tenancy_ocid="ocid1.tenancy.oc1..aaaaaaaaeajdi3ywexve5hkkbya4pzw2vlbnras42gv5l5ssm7rr66lvrpva"
export TF_VAR_user_ocid="ocid1.user.oc1..aaaaaaaaagu7f63nyadsj5nd2uhfjnp6shpfli7lzsidxyrcg7qswzsnr6wq"
export TF_VAR_fingerprint="3b:36:0b:4c:17:e1:ab:df:89:3a:4c:0b:1b:f1:33:8c"
export TF_VAR_private_key_path="/Users/sukesh/.oci/suchulli_oci_psmqaus.pem"
export TF_VAR_region="us-ashburn-1"
export OCI_PROVIDER_NAME="terraform-provider-oci_v4.45.0_x4"
mkdir -p $HOME/resource-discovery


$OCI_PROVIDER_NAME -command=export -compartment_name="TerraformComp" -services=$1 -output_path=$HOME/resource-discovery
$OCI_PROVIDER_NAME -command=export -compartment_name="TerraformComp" -services=$1 -generate_state -output_path=$HOME/resource-discovery
