#/bin/bash
Help()
{
 echo "please run the command with single parameter as a name for your key"
 ##
 # Run this command with keyname of your choice as parameter, you get public key
 # Copy this to OCI console API keys (users>select your username> left panel you see 
 # API sigining keys . Paste the public key )
 ##
}
export KEY_NAME=$1
echo "Name Prefix for key is ${KEY_NAME}"
mkdir -p $HOME/.oci/
openssl genrsa -out $HOME/.oci/${KEY_NAME}.pem 2048
chmod 600 $HOME/.oci/${KEY_NAME}.pem
openssl rsa -pubout -in $HOME/.oci/${KEY_NAME}.pem -out $HOME/.oci/${KEY_NAME}_public.pem
cat $HOME/.oci/${KEY_NAME}_public.pem
