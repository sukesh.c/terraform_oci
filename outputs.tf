   # Output the "list" of all availability domains.
output "all-availability-domains-in-your-tenancy" {
  value = data.oci_identity_availability_domains.ads.availability_domains
}
output "compartment_name" {
    value = oci_identity_compartment.compartment.name
  
}
output "compartment-OCID" {
    value = oci_identity_compartment.compartment.id
  
}