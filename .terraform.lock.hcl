# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/oci" {
  version     = "4.46.0"
  constraints = "~> 4.44"
  hashes = [
    "h1:hGf+fTM5wlNzvB6qI70r9FMEq1P4dM4ZfG8QJ6nJ7MQ=",
    "zh:05bca775a6cb8534d0edec24985d2dfbfe07cd54d05db5721efd7755ef7c1b94",
    "zh:0e3dc9141bdf7109d6badf92b47c356d1be63502046c485c3c2629a7aeae292a",
    "zh:22380194af49add69325bd069b3b56f48f280d6258a8e3ebe1a4fd78c05e3879",
    "zh:3a7f9ffb14b6f56b1c2cdf0c6ac3d4e3399eee00a511bb9d4b36c35e4065f335",
    "zh:636ac0f2f223bd917a728ef2a48f2d1f88f73e720f62029e3b82aa07dfe00aa9",
    "zh:7658b355f2d2fe09b0ea523041c7edb69e747926464320a6f2b9e6fa549fa046",
    "zh:923fb093d970baffaf67033dbc07673eb15f3f7b49bc05b7714f587de6f0f086",
    "zh:da37df79a8b20fec53971bd9e87b2185c47bcc6817707bd1a28a89d19391ca46",
    "zh:f09d3c284799da5b35e7f25850eac21bf112aa46a8674690dbf5af343c072dc2",
    "zh:fd0859e5a92eb68fdc371ad7ff5f0305c47cb3ca2f0072bb939263c38618e793",
  ]
}
