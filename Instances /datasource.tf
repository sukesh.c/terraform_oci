



# get all AD
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.tocid
}
#get images "
data "oci_core_images" "oci_images" {
    #Required
   compartment_id = var.tocid

    #Optional
   //display_name = var.os_disply_name
    operating_system = var.os_name
  operating_system_version = var.os_version
    shape = var.image_shape
    #state = var.image_state
    sort_by = var.image_sort_by
    sort_order = var.sort_order
}
