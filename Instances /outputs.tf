   # Output the "list" of all availability domains.
output "all-availability-domains-in-your-tenancy" {
  value = data.oci_identity_availability_domains.ads.availability_domains.1.id
}
output "compartment_name" {
    value = oci_identity_compartment.compartment.name
  
}
output "compartment-OCID" {
    value = oci_identity_compartment.compartment.id
  
}
output "images_list" {
  value = data.oci_core_images.oci_images.images.0.id
}