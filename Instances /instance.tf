
#Compartment 
resource "oci_identity_compartment" "compartment" {
   
    compartment_id = var.tocid
    description    = "Compartment for Terra VCN."
    name           = "TerraComp"
    enable_delete  = true
}
 output "comp_ocid" {
     value = oci_identity_compartment.compartment.id
 }

#VCN
resource "oci_core_vcn" "terraform-vnc" {

    compartment_id     = oci_identity_compartment.compartment.id
    //cidrBlocks         = var.cider
     cidr_block        = var.cider
    display_name       = var.vcn_name
    dns_label          = var.dns_label
  
}
#Internet Gateway
resource "oci_core_internet_gateway" "terraig" {
    compartment_id          = oci_identity_compartment.compartment.id
    vcn_id                  = oci_core_vcn.terraform-vnc.id
            enabled            = var.ig_enabled 
            display_name = var.igname
  
}
#Route Table
resource "oci_core_route_table" "terraroutetable" {
  compartment_id = oci_identity_compartment.compartment.id
  vcn_id = oci_core_vcn.terraform-vnc.id
  display_name =var.rtname
  route_rules {
      network_entity_id = oci_core_internet_gateway.terraig.id
      description = "terra route table"
      //cidr_block = var.public_rule_cider
      destination = var.public_rule_cider
      destination_type = "CIDR_BLOCK"
    
  }
  
}
#subnet
resource "oci_core_subnet" "terrasubnet" {
  cidr_block      = var.subnet_cider
  compartment_id  = oci_identity_compartment.compartment.id
  vcn_id          = oci_core_vcn.terraform-vnc.id
  display_name = var.subnet_name_pub
  route_table_id  = oci_core_route_table.terraroutetable.id
}


#instance
resource "oci_core_instance" "sukeshTest" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[1].name
    compartment_id = oci_identity_compartment.compartment.id
    shape = var.image_shape
    shape_config {
      baseline_ocpu_utilization = var.instance_shape_config_baseline_ocpu_utilization
      memory_in_gbs = var.instance_shape_config_memory_in_gbs
      ocpus = var.instance_shape_config_ocpus
    }
    display_name = "SukeshTestInst"
    metadata = {ssh_authorized_keys= file("/Users/sukesh/keys/xperiment_rsa.pub")}
    
  source_details {
        #Required
        source_id = data.oci_core_images.oci_images.images.0.id
        source_type = "image"

}

create_vnic_details {
  assign_public_ip =true
  subnet_id=oci_core_subnet.terrasubnet.id
  
}
preserve_boot_volume = false
}