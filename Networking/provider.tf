terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
      version = "~>4.4"
    }
  }
}

provider "oci" {
 tenancy_ocid = var.tocid
 user_ocid = var.uocid
 fingerprint = var.fp
 private_key_path =var.pvtkey
 region=var.region

}