
#Compartment 
resource "oci_identity_compartment" "compartment" {
   
    compartment_id = var.tocid
    description    = "Compartment for Terra VCN."
    name           = "TerraComp"
    enable_delete  = true
}
 output "comp_ocid" {
     value = oci_identity_compartment.compartment.id
   
 }

#VCN
resource "oci_core_vcn" "terraform-vnc" {

    compartment_id     = oci_identity_compartment.compartment.id
     cidr_block        = var.cider
    display_name       = var.vcn_name
    dns_label          = var.dns_label

  
}
#Internet Gateway
resource "oci_core_internet_gateway" "terraig" {
    compartment_id          = oci_identity_compartment.compartment.id
    vcn_id                  = oci_core_vcn.terraform-vnc.id
            enabled            = var.ig_enabled 
            display_name = var.igname
  
}
#Route Table
resource "oci_core_route_table" "terraroutetable" {
  compartment_id = oci_identity_compartment.compartment.id
  vcn_id = oci_core_vcn.terraform-vnc.id
  display_name =""
  route_rules {
      network_entity_id = oci_core_internet_gateway.terraig.id
      description = "terra route table"
      cidr_block = var.public_rule_cider
      //destination = var.public_rule_cider
      //destination_type = "Internet"
    
  }
  
}
#subnet
resource "oci_core_subnet" "terrasubnet" {
  cidr_block      = var.subnet_cider
  compartment_id  = oci_identity_compartment.compartment.id
  vcn_id          = oci_core_vcn.terraform-vnc.id
  route_table_id  = oci_core_route_table.terraroutetable.id
}