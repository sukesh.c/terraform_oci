# variable "environment" { default="dev"}
# variable "shpare" {
#     type="map"
#     default = {
#         dev="VM.Standard1.2"
#         test="VM.Standard1.2"
#         prod="VM.Standard1.2"
#     }

# }
variable tocid {
  description = "OCID for tenancy "
  type        = string
  default = "ocid1.tenancy.oc1..aaaaaaaaeajdi3ywexve5hkkbya4pzw2vlbnras42gv5l5ssm7rr66lvrpva"
 
}
variable uocid {
  description = "OCID for User "
  type        = string
  default = "ocid1.user.oc1..aaaaaaaaagu7f63nyadsj5nd2uhfjnp6shpfli7lzsidxyrcg7qswzsnr6wq"
  
}

 
variable compartment_ocid { 
  default = "ocid1.compartment.oc1..aaaaaaaa6pqhxrumodextlbmmheaw7y6wt4dmaxpkcbi4qij4e6dmf6l4sfq" 
  }
variable "fp" {
  description = "FingerPrint  for User "
  type        = string
  default = "3b:36:0b:4c:17:e1:ab:df:89:3a:4c:0b:1b:f1:33:8c"
}
variable "pvtkey" {
  description = "Private Key Path  "
  type        = string
  default = "/Users/sukesh/.oci/suchulli_oci_psmqaus.pem"
}
 variable "pubkey" {
 description = "Public   Key Path  "
  type        = string
  default = "/Users/sukesh/.oci/suchulli_oci_psmqaus_public.pem"
 }
 variable "region" {
 description = "Public   Key Path  "
  type        = string
  default = "us-ashburn-1"
 }
######### Networking Variables ######

variable cider {
 default = "10.0.0.0/16"

}
variable vcn_name {
  default = "Terravcn"
}
variable subnet_cider {
 default = "10.0.0.0/24"
}
variable dns_label {
default = "terravcn"  
}
variable "ig_enabled" {
default = "true"  
}
variable "public_rule_cider" {
  #type = list
  #default = ["0.0.0.0/0"]
  default = "0.0.0.0/0"
  
}
variable "igname" {
  default = "TerraIG"
}

  
