
resource "oci_identity_compartment" "compartment" {
   
    compartment_id = var.tocid
    description = "Compartment for Terraform resources."
    name = "TerraformComp"
}